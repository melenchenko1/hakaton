@component('mail::message')

# {{__("Booking request")}}

@foreach ($introLines as $line)
{{ $line }}

@endforeach

@component('mail::button', ['url' => $confirmUrl, 'color' => 'success'])

{{__("Confirm booking")}}

@endcomponent
@component('mail::button', ['url' => $declineUrl, 'color' => 'error'])

{{__("Decline booking")}}

@endcomponent

{{ $salutation }}
@endcomponent
