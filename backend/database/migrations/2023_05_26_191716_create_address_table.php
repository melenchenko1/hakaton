<?php

use App\Models\Address;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->enum('entity_type', Address::ENUM_ENTITY);
            $table->bigInteger('entity_id', false, true);
            $table->string('city', 50)->nullable();
            $table->text('street')->nullable();
            $table->text('house')->nullable();
            $table->text('path_description')->nullable();
            $table->index(['entity_type', 'entity_id', 'city']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address');
    }
}
