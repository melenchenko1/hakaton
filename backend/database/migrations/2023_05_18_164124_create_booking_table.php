<?php

use App\Models\Booking;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('room_id')->constrained();
            $table->enum('status', Booking::ENUM_STATUS);
            $table->timestamp('start_time');
            $table->timestamp('end_time');
            $table->foreignId('contract_id')->nullable()->constrained();
            $table->index(['user_id', 'start_time']);
            $table->index(['room_id', 'start_time']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking');
    }
}
