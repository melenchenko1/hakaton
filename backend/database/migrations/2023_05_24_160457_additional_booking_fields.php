<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AdditionalBookingFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('buildings', function (Blueprint $table) {
            $table->text('description')->nullable();
            $table->integer('population')->nullable();
        });
        Schema::table('rooms', function (Blueprint $table) {
            $table->text('description')->nullable();
            $table->jsonb('schedule')->nullable();
            $table->text('floor')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
