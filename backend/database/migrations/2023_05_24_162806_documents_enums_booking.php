<?php

use App\Models\Document;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DocumentsEnumsBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $enumString = sprintf("'%s'", implode("', '", Document::ENUM_ENTITY));
        DB::statement(
            "ALTER TABLE documents MODIFY COLUMN `documentable_type` ENUM($enumString)"
        );
        $enumString = sprintf("'%s'", implode("', '", Document::ENUM_TYPE));
        DB::statement(
            "ALTER TABLE documents MODIFY COLUMN `type` ENUM($enumString)"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
