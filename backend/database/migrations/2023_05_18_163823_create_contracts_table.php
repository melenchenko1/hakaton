<?php

use App\Models\Contract;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('contracts');
        Schema::create('contracts', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->enum('status', Contract::ENUM_STATUS);
            $table->text('file')->nullable();
            $table->text('signed_file')->nullable();
            $table->enum('type', Contract::ENUM_TYPE);
            $table->foreignId('user_id')->constrained();
            $table->bigInteger('user2_id', false, true);
            $table->foreign('user2_id')->references('id')->on('users');
            $table->index(['user_id', 'type']);
            $table->index(['user2_id', 'type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
