<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class InitRequisiteKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('requisite_keys')->insert([
            ['label' => 'Полное наименование организации'],
            ['label' => 'Краткое наименование организации'],
            ['label' => 'Юридический адрес'],
            ['label' => 'Фактический адрес'],
            ['label' => 'Телефон'],
            ['label' => 'Факс'],
            ['label' => 'Электронная почта'],
            ['label' => 'ИНН'],
            ['label' => 'КПП'],
            ['label' => 'ОГРН / ОГРНИП'],
            ['label' => 'ОКПО'],
            ['label' => 'р/с'],
            ['label' => 'к/с'],
            ['label' => 'БИК'],
            ['label' => 'Банк'],
            ['label' => 'Должность подписанта'],
            ['label' => 'Должность подписанта (в родительном падеже)'],
            ['label' => 'ФИО подписанта'],
            ['label' => 'ФИО подписанта (в родительном падеже)'],
            ['label' => 'Действует на основании'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
