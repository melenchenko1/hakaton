<?php

namespace App\Notifications\Traits;

use App\Models\Booking;
use Illuminate\Notifications\Messages\MailMessage;

trait infoFromBooking
{
    private function fromBookingToMail(MailMessage $message, Booking $booking): void
    {
        $message->line(__('Booking information'));
        $message->line($booking->room->building->name);
        $message->line(__("Address: :address", ['address' => $booking->room->building->name]));
        $message->line(__("Space: :space", ['space' => $booking->room->number]));
        $message->line(__("Booking start time: :time", ['time' => $booking->start_time]));
        $message->line(__("Booking end time: :time", ['time' => $booking->end_time]));
    }
}
