<?php

namespace App\Notifications;

use App\Models\Booking;
use App\Notifications\Traits\infoFromBooking;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\URL;

class BookingCreated extends Notification
{
    use Queueable, infoFromBooking;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(private readonly Booking $booking)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = (new MailMessage)
            ->subject(__('Booking request') . ': ' . $this->booking->room->number)
            ->greeting(__('Booking request'));
        $this->fromBookingToMail($message, $this->booking);
        $message->markdown('booking.mail-actions', [
                'confirmUrl' => URL::signedRoute(
                    'booking-confirm',
                    ['booking_id' => $this->booking->id]
                ),
                'declineUrl' => URL::signedRoute(
                    'booking-decline',
                    ['booking_id' => $this->booking->id]
                )
            ])
            ->salutation(__("Sincerely, :site", ['site' => config('app.name')]));
        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
