<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Room;
use App\Models\User;
use App\Notifications\BookingCreated;
use DateTimeImmutable;
use Eluceo\iCal\Domain\Entity\Attendee;
use Eluceo\iCal\Domain\Entity\Calendar;
use Eluceo\iCal\Domain\Entity\Event;
use Eluceo\iCal\Domain\ValueObject\Category;
use Eluceo\iCal\Domain\ValueObject\DateTime;
use Eluceo\iCal\Domain\ValueObject\EmailAddress;
use Eluceo\iCal\Domain\ValueObject\Location;
use Eluceo\iCal\Domain\ValueObject\Organizer;
use Eluceo\iCal\Domain\ValueObject\TimeSpan;
use Eluceo\iCal\Domain\ValueObject\Uri;
use Eluceo\iCal\Presentation\Factory\CalendarFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BookingController extends Controller
{
    public function store(Request $request)
    {
        $room = Room::findOrFail($request->room_id);
        if (!Booking::isFree($request->start_time, $request->end_time, $room)) {
            abort(Response::HTTP_CONFLICT, 'This interval is already booked');
        }
        $isSelfBooking = $request->user()->id == $room->building->user_id;
        $booking = new Booking([
            'user_id' => User::root()->id,
            'room_id' => $room->id,
            'status' => $isSelfBooking ? Booking::STATUS_ACCEPTED : Booking::STATUS_NEW,
            'start_time' => $request->start_time,
            'end_time' => $request->end_time
        ]);
        $request->has('description') && $booking->description = $request->description;
        $booking->save();
        if (!$isSelfBooking) {
            $booking->room->building->user->notify(new BookingCreated($booking));
        }
        return $booking->id;
    }

    public function index(Request $request)
    {
        $user = User::root();
        $query = Booking::with('room.building');
        if ($user->type == User::TYPE_BUILDING) {
            $query = $query->whereRelation('room.building', 'user_id', $user->id);
        } else {
            $query = $query->where('user_id', $user->id);
        }
        $request->has('building_id') && $query = $query->whereRelation('room', 'building_id', $request->building_id);
        $request->has('status') && $query = $query->where('status', $request->status);
        $request->has('start_time_to') && $query = $query->where('start_time', '<', $request->start_time_to);
        $request->has('end_time_from') && $query = $query->where('end_time', '>', $request->end_time_from);
        return $query->get();
    }

    public function exportIcs(Request $request)
    {
        $calendar = new Calendar();
        foreach ($this->index($request) as $booking) {
            /* @var Booking $booking */
            $building = $booking->room->building;
            $event = new Event();
            $event->setSummary($building->name);
            $event->setUrl(new Uri(config('portal.frontend_url') . config('deeplinks.booking')));
            $start = $booking->start_time->format('Y-m-d H:i:s');
            $end = $booking->end_time->format('Y-m-d H:i:s');
            $occurence = new TimeSpan(
                new DateTime(DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $start), false),
                new DateTime(DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $end), false)
            );
            $event->setOccurrence($occurence);
            $event->setLocation(new Location($building->address . ', ' . $booking->room->number));
            if (User::root()->type === User::TYPE_BUILDING) {
                $attendeeEmail = $booking->user->email;
                if (filter_var($attendeeEmail, FILTER_VALIDATE_EMAIL)) {
                    $attendee = new Attendee(new EmailAddress($attendeeEmail));
                    $attendee->setDisplayName($booking->user->name ?? '');
                    $event->setAttendees([$attendee]);
                }
            } else {
                $buildingEmail = $building->contacts->first()?->email ?? $building->email ?? $building->user->email;
                if (filter_var($buildingEmail, FILTER_VALIDATE_EMAIL)) {
                    $event->setOrganizer(new Organizer(
                        new EmailAddress($buildingEmail),
                        $building->contacts->first()?->person
                    ));
                }
            }
            $event->addCategory(new Category('NAVICONF'));
            $calendar->addEvent($event);
        }
        $componentFactory = new CalendarFactory();
        $calendarComponent = $componentFactory->createCalendar($calendar);
        return response()->streamDownload(function () use ($calendarComponent) {
            echo $calendarComponent;
        }, 'naviconf.ics', ['Content-Type' => 'text/calendar']);
    }
}
