<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FeedbackController extends Controller
{
    public function store(Request $request)
    {
        $feedback = new Feedback([
            'user_id' => Auth::user()->id,
            ...$request->only('entity_type', 'entity_id', 'score'),
        ]);
        $request->has('report') && $feedback->report = $request->report;
        $feedback->save();
        return $feedback->id;
    }

    public function index(Request $request)
    {
        if (empty($request->only(['entity_type', 'entity_id']))) {
            return Feedback::where('user_id', Auth::user()->id)->get();
        } else {
            return Feedback::where('entity_type', $request->entity_type)->where('entity_id', $request->entity_id)->get();
        }
    }
}
