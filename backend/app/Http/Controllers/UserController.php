<?php

namespace App\Http\Controllers;

use App\Models\Requisite;
use App\Models\RequisiteKey;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function view(Request $request)
    {
        $result = $request->user()->load('anketa')->load('requisites')->toArray();
        $result['all_requisites'] = [];
        foreach (RequisiteKey::all() as $requisite) {
            $result['all_requisites'][$requisite->id] = $requisite->label;
        }
        return $result;
    }

    public function update(Request $request)
    {
        $user = $request->user();
        foreach ($request->requisites as $requisiteData) {
            $requisite = $requisiteData['action'] === 'insert'
                ? new Requisite(['user_id' => $user->id, 'requisite_key_id' => $requisiteData['requisite_key_id']])
                : Requisite::findOrFail($requisiteData['id']);
            switch ($requisiteData['action']) {
                case 'delete':
                    $requisite->delete();
                    break;
                case 'update':
                case 'insert':
                default:
                    $requisite->value = $requisiteData['value'];
                    $requisite->is_default = $requisiteData['is_default'];
                    $requisite->save();
            }
        }
        return $this->view($request);
    }
}
