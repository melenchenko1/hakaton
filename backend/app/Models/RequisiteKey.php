<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequisiteKey extends Model
{
    use HasFactory;

    public function requisites()
    {
        return $this->hasMany(Requisite::class);
    }
}
