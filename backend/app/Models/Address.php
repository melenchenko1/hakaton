<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Address extends Model
{
    use HasFactory;

    protected $table = 'address';
    public const ENUM_ENTITY = [Building::MORPH_TYPE];

    public function entity(): MorphTo
    {
        return $this->morphTo();
    }
}
