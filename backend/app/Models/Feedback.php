<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @property int $id
 * @property ?string $report
 */
class Feedback extends Model
{
    use HasFactory;

    protected $table = 'feedback';

    public const ENUM_ENTITY = [Building::MORPH_TYPE];

    protected $fillable = ['user_id', 'entity_type', 'entity_id', 'score'];

    public function entity(): MorphTo
    {
        return $this->morphTo();
    }
}
