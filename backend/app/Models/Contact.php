<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @property ?string $person
 * @property ?string $phone
 * @property int $owner_id
 * @property string $entity_type
 * @property int $entity_id
 * @property int $id
 * @property Model $entity
 * @method static self create(array $fields)
 */
class Contact extends Model
{
    use HasFactory;

    public const MORPH_TYPE = 'contact';
    public const ENUM_ENTITY = [Building::MORPH_TYPE];
    public const TYPES = ['person', 'phone', 'occupation', 'email'];
    public const VALIDATOR = [
        'person' => ['string', 'max:100'],
        'phone' => ['string', 'max:50'],
        'email' => ['email'],
        'occupation' => ['string', 'max:100'],
    ];

    protected $fillable = ['owner_id', 'entity_type', 'entity_id', ...self::TYPES];
    /**
     * @return MorphTo
     */
    public function entity(): MorphTo
    {
        return $this->morphTo();
    }

    public function owner(): BelongsTo
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function documents(): MorphMany
    {
        return $this->morphMany(Document::class, 'documentable');
    }
}
