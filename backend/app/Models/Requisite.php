<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Requisite extends Model
{
    use HasFactory;

    protected $fillable = ['requisite_key_id', 'value', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function key()
    {
        return $this->hasOne(RequisiteKey::class);
    }
}
