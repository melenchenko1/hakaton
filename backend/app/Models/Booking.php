<?php

namespace App\Models;

use App\Notifications\BookingAccepted;
use App\Notifications\BookingRejected;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Notification;

/**
 * @property User $user
 * @property int $id
 * @property Room $room
 * @property ?string $description
 * @property mixed $start_time
 * @property mixed $end_time
 * @method static self findOrFail(mixed $booking_id)
 */
class Booking extends Model
{
    public const STATUS_NEW = 'new';
    public const STATUS_ACCEPTED = 'accepted';
    private const STATUS_REJECT = 'rejected';
    public const ENUM_STATUS = [self::STATUS_NEW, self::STATUS_ACCEPTED, self::STATUS_REJECT];

    protected $fillable = ['user_id', 'room_id', 'status', 'start_time', 'end_time'];
    protected $table = 'booking';
    protected $casts = [
        'start_time' => 'datetime',
        'end_time' => 'datetime',
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function accept()
    {
        $this->status = self::STATUS_ACCEPTED;
        $this->save();
        $this->user->notify(new BookingAccepted($this));
    }

    public function reject()
    {
        $this->status = self::STATUS_REJECT;
        $this->save();
        $this->user->notify(new BookingRejected($this));
    }

    public static function isFree(string $startTime, string $endTime, Room $room): bool
    {
        return !self::where('start_time', '<', $endTime)->where('end_time', '>', $startTime)->exists()
            && $room->isFree(Carbon::parse($startTime), Carbon::parse($endTime));
    }
}
