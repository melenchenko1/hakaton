import "./App.css";
import React from "react";
import { Router, Route } from "react-router-dom";
import { PrivateRoute } from "./_components";
import { history } from "./_helpers";
import { MainPage } from "./MainPage";
import { LoginPage, RegistrPage } from "./LoginPage";
import AgrPage from "./LoginPage/AgrPage";
import VerifyPage from "./LoginPage/VerifyPage";
import PasswordResetPage from "./LoginPage/PasswordResetPage";
import PasswordForgotPage from "./LoginPage/PasswordForgotPage";
import ModerationPage from "./LoginPage/ModerationPage";

const routes = ["/settings", "/new_site", "/booking"];

const routes_id = ["/site_param/:id", "/site_view/:id"];

export class App extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    document.title = "Naviconf";
  }

  render() {
    return (
      <Router history={history}>
        <Route path="/login" component={LoginPage} />
        <Route path="/registration" component={RegistrPage} />
        <Route path="/agreement" component={AgrPage} />
        <Route path="/email-verified" component={VerifyPage} />
        <Route path="/password-reset" component={PasswordResetPage} />
        <Route path="/password-forgot" component={PasswordForgotPage} />
        <PrivateRoute exact path="/" component={MainPage} />
        <PrivateRoute path="/purchased-:pid" component={MainPage} />
        <Route path="/ticket-moderation" component={ModerationPage} />
        {routes.map((item, index) => (
          <PrivateRoute exact path={item} component={MainPage} key={index} />
        ))}
        {routes_id.map((item, index) => (
          <PrivateRoute path={item} component={MainPage} key={1000 + index} />
        ))}
      </Router>
    );
  }
}

export default App;
