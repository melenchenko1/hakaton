import { userService } from "./user.service";
import { orgConferencesActions, teamActions, userActions } from "../_actions";
import { store } from "../_helpers";

const load = (url, action) => {
  store.dispatch(userActions.loading(true));
  userService.getWrapper(
    url,
    (data) => {
      store.dispatch(userActions.loading(false));
      if (data.data) {
        store.dispatch(action(data.data));
      }
    },
    (error) => {
      store.dispatch(userActions.loading(false));
      if (error !== "") {
        //alert(error);
        console.log(error);
      }
    }
  );
};

export const reduxLoader = {
  orgConferences: () => load("api/conference", orgConferencesActions.load),
  team: () => load("api/team?withOrg=1", teamActions.load),
};
