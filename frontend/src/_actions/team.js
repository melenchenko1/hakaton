import { teamConstants } from "../_constants";

export const teamActions = {
  load,
};

function load(data) {
  return { type: teamConstants.LOAD, data };
}
