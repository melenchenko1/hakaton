export const storeData = { company: null, companies: [], products: [] };

export const setStore = (name, data) => {
  storeData[name] = data;
};
