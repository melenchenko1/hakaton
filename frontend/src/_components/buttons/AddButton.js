import React from "react";
import { SvgButton } from "./SvgButton";

export class AddButton extends SvgButton {
  handleClick = (e) => {
    if (this.props.onClick) {
      e.stopPropagation();
      this.props.onClick(e);
    }
  };

  render() {
    const { id, name, size } = this.props;
    return (
      <SvgButton
        id={id}
        name={name || "Добавить"}
        size={size}
        box="0 0 304 512"
        path="M376 232H216V72c0-4.42-3.58-8-8-8h-32c-4.42 0-8 3.58-8 8v160H8c-4.42 0-8 3.58-8 8v32c0 4.42 3.58 8 8 8h160v160c0 4.42 3.58 8 8 8h32c4.42 0 8-3.58 8-8V280h160c4.42 0 8-3.58 8-8v-32c0-4.42-3.58-8-8-8z"
        onClick={this.props.onClick && this.handleClick}
      />
    );
  }
}
