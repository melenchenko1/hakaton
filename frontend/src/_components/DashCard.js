import React from "react";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import { withStyles } from "@material-ui/core/styles";

const styles = {
  text: {
    fontSize: "17px",
    fontWeight: 500,
    color: "#0b1218",
    marginBottom: "16px",
  },
  bigNumber: {
    fontSize: "28px",
    fontWeight: "bold",
    color: "#0b1218",
  },
  label: {
    fontSize: "17px",
    fontWeight: "normal",
    color: "rgba(11, 18, 24, 0.46)",
    marginLeft: "8px",
  },

  card: {
    borderRadius: "4px",
    width: "100%",
    padding: "4px",
    height: "110px",
    backgroundColor: "#ffffff",
    display: "flex",
    flexFlow: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
  },
};

class DashCard extends React.Component {
  handleClick = (e) => {
    this.props.onClick(e);
  };

  render() {
    const { id, classes } = this.props;
    return (
      <div>
        <Card style={{ boxShadow: "0 2px 24px 0 rgba(0, 0, 0, 0.06)" }}>
          <CardActionArea
            id={id}
            className={classes.card}
            onClick={this.handleClick}
          >
            <CardContent>
              <div className={classes.text}>{this.props.text}</div>
              <div className={classes.bigNumber}>
                {this.props.title}
                {this.props.label && (
                  <span className={classes.label}> {this.props.label} </span>
                )}
              </div>
            </CardContent>
          </CardActionArea>
        </Card>
      </div>
    );
  }
}

const styledDashCard = withStyles(styles, { withTheme: true })(DashCard);
export { styledDashCard as DashCard };
