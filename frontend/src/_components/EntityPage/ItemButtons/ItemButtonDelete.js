import React from "react";
import i18next from "i18next";
import DeleteIcon from "@material-ui/icons/Delete";

export default class ItemButtonDelete extends React.Component {
  render() {
    return (
      <div title={i18next.t("Delete")} style={{ width: 20, marginLeft: 10 }}>
        <DeleteIcon
          width="15"
          height="15"
          onClick={(e) => this.props.action(this.props.id)}
        />
      </div>
    );
  }
}
