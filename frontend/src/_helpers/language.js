import i18next from "i18next";

export const initLanguage = () => {
  i18next.init({
    lng: "ru", //ToDo: get from cookies
    resources: require("../_lang/ru.json"),
  });
};
