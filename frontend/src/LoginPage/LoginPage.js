import React from "react";
import { Button, Grid, CircularProgress } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import { connect } from "react-redux";

import CustomizedButton from "../_components/buttons/CustomizedButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import IconButton from '@mui/material/IconButton';

import { SendButton, TextFieldCell, Snack } from "../_components";
import { history, initLanguage, store } from "../_helpers";

import CookiePopup from "../MainPage/CookiePopup";
import { userActions } from "../_actions";
import { serverUrl, agrUrl } from "../_constants";
import "../css/style.css";
import i18next from "i18next";

function getCookie(name) {
  const matches = document.cookie.match(
    new RegExp(
      `(?:^|; )${name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1")}=([^;]*)`
    )
  );
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

const loginIsEmail = process.env.REACT_APP_LOGIN_IS_EMAIL;


class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    // reset login status

    this.state = {
      username: "",
      pwd: "",
      submitted: false,
      alertShow: false,
      err_type: "",
      snackText: "",
	  showPwd: false,
    };

    store.subscribe(this.storeChange);
  }

  storeChange = (e) => {
    if (
      store.getState().authentication.error &&
      store.getState().authentication.isLoading == false
    ) {
      this.setState({
        snackText: "Неправильные "+(loginIsEmail?'email':'логин')+" пользователя или пароль",
        alertShow: true,
        err_type: "error",
      });
    }
  };

  componentDidMount() {
    const { dispatch } = this.props;

    const username = localStorage.getItem("navic_user");
    const pwd = localStorage.getItem("navic_pwd");

    if (username && pwd) {
      this.setState({ username, pwd });
      dispatch(userActions.login({ username, password: pwd }));
    }
  }

  onCloseSnack = () => {
    this.setState({ alertShow: false });
  };

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  openReg = () => {
    // alert(window.location.href);
    history.push("/registration");
  };

  handleSubmit = (e) => {
    const self = this;

    e.preventDefault();

    this.setState({ submitted: true });
    const { username, pwd } = this.state;
    const { dispatch } = this.props;

    if (username.length > 0 && pwd.length > 0) {
      localStorage.setItem("user", "{}");

      dispatch(userActions.login({ username, password: pwd }));
    } else {
      this.setState({
        snackText: "Введите "+(loginIsEmail?'email':'логин')+" и пароль",
        alertShow: true,
        err_type: "warning",
      });
    }
  };

  render() {
    initLanguage();

    const { error, isLoading } = this.props;

    const {
      username,
      submitted,
      pwd,
      alertShow,
      err_type,
      snackText,
	  showPassword,
    } = this.state;

    let errorMsg = "Неверный токен";
    if (error) {
      if (error === "Forbidden") errorMsg = "Неправильный токен";
    }

    return (
      <div style={{ width: "100%", height: "100%", verticalAlign: "center" }}>
        <div style={{ width: 400, margin: "0 auto", paddingTop: "30vh" }}>
          <form name="form" onSubmit={this.handleSubmit}>
            <div className={submitted && !username ? " has-error" : ""}>
              <TextFieldCell
                id="login"
				label={loginIsEmail?'Email пользователя':"Логин пользователя"}
                name="username"
                type="text"
                value={username}
                onChange={this.handleChange}
				autoFocus
              />
            </div>
            <div className={submitted && !username ? " has-error" : ""}>
              <TextField
				style={{display:'flex',marginBottom:20}}
                id="pwd"
                label="Пароль"
                name="pwd"
                type={showPassword?"text":"password"}
                value={pwd}
                onChange={this.handleChange}
				 InputProps={{
						style:{fontSize: 24,    
						fontWeight: "bold",
						color: "#0b1218"},
						 endAdornment:  
						   <InputAdornment position="end">
							  <IconButton
								onClick={()=>this.setState({showPassword:!showPassword})}
								edge="end"
							  >
								{showPassword ? <Visibility /> : <VisibilityOff />}
							  </IconButton>
							</InputAdornment>
						 
					   }}
              />
            </div>

            <div
              className="form-group"
              style={{
                marginTop: 5,
                justifyContent: "space-between",
                flex: 1,
                display: "flex",
              }}
            >
              <CustomizedButton
                style={{ marginBottom: 5 }}
                id="category-button-add"
                type="submit"
                title="Войти"
                prim
              />
              <CustomizedButton
                style={{ marginBottom: 5 }}
                onClick={this.openReg}
                id="category-button-add"
                title="Регистрация"
                prim
              />
            </div>
          </form>
          <a href="/password-forgot" className="forgot-password-link">{i18next.t("Forgot password")}</a>
          
        </div>

        {isLoading && (
          <div className="shadow">
            <div className="shadow_circle">
              <CircularProgress />
            </div>
          </div>
        )}
        <CookiePopup />
        <Snack
          open={alertShow}
          variant={err_type}
          text={snackText}
          onClose={this.onCloseSnack}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { loggingIn, error, isLoading } = state.authentication;
  return {
    loggingIn,
    isLoading,
    error,
  };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage };
