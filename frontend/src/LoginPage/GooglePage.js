import React from "react";
import { Button, Grid } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";

import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import InputLabel from "@mui/material/InputLabel";

import { connect } from "react-redux";
import { SendButton, TextFieldCell } from "../_components";
import { history } from "../_helpers";
import { userService } from "../_services";
import CustomizedButton from "../_components/buttons/CustomizedButton";
import { userActions } from "../_actions";
import { store } from "../_helpers";

function getCookie(name) {
  const matches = document.cookie.match(
    new RegExp(
      "(?:^|; )" +
        name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") +
        "=([^;]*)"
    )
  );
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setStorage(name,val)
{
	localStorage.setItem(name,val);
}

function setCookie(name, value, options) {
  options = options || {};
  let { expires } = options;
  if (typeof expires === "number" && expires) {
    let d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }

  if (expires && expires.toUTCString) options.expires = expires.toUTCString();

  value = encodeURIComponent(value);
  let updatedCookie = `${name}=${value}`;
  for (const propName in options) {
    updatedCookie += `; ${propName}`;
    const propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += `=${propValue}`;
    }
  }

  document.cookie = updatedCookie;
}

class GooglePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: "",
      pwd: "",
      submitted: false,
      getCb: false,
      isNew: false,

      agreeOk: false,
      slotInfoOpen: false,
    };

    store.subscribe(this.storeChange);
  }

  storeChange = (e) => {
    console.log(e);
    console.log(store.getState());
    /*
        if (store.getState().authentication.error && store.getState().authentication.isLoading == false)
            alert(store.getState().authentication.error);

        */
  };

  componentDidMount() {
    const { dispatch } = this.props;

    const { isNew } = this.state;

    const self = this;
    let str = window.location.href;

    let userType = "org";

    console.log(str);

    if (str.indexOf("callback-s") >= 0) userType = "student";

    let str_arr = str.split("code=");
    if (str_arr.length > 1) {
      str = str_arr[1];
      str_arr = str.split("&scope");
      str = str_arr[0];

      localStorage.setItem("user", "{}");

      userService.getWrapper(
        `api/token/google?type=${userType}&code=${str}`,
        (data) => {
          console.log(data);
          if (data && data.data && data.data.token) {
            if (data.data.is_agreement_accepted == false) {
              setStorage("token_tmp", data.data.token);
              setStorage("token", "");
              history.push("/agreement");
            } else {
              // localStorage.setItem('navic_agree','1');
              setStorage("token", data.data.token);
              history.push("/");
            }
          }
        },
        (error) => {
          if (error != "") {
            self.setState({ error, isLoading: false });
            alert(error);
          }
        }
      );
    }
  }

  clickAgr = () => {
    // this.setState({slotInfoOpen:true});
    window.open("https://naviconf.com/privacy-policy");
  };

  handleSubmit = (e) => {
    const { getCb, isNew, agreeOk, token, id } = this.state;

    if (agreeOk) {
      localStorage.setItem(`navic_agree_${id}`, "1");
      setStorage("token", token);
      history.push("/");
    }
  };

  render() {
    const {
      getCb,
      isNew,
      slotInfoOpen,
      agreeOk,
      snackText,
      err_type,
      alertShow,
      userType,
    } = this.state;

    return <div>Loading...</div>;
  }
}

function mapStateToProps(state) {
  const { loggingIn, error } = state.authentication;
  return {
    loggingIn,
    error,
  };
}

const connectedGooglePage = connect(mapStateToProps)(GooglePage);
export { connectedGooglePage as GooglePage };
