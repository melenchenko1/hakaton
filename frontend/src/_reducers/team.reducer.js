import { teamConstants } from "../_constants";

const initialState = [];

export function team(state = initialState, action) {
  switch (action.type) {
    case teamConstants.LOAD:
      return action.data;
    default:
      return state;
  }
}
