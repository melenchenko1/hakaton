import { userConstants } from "../_constants";

let user = null;

try {
  // let token = 'token';
  // user = JSON.parse(localStorage.getItem('token'));
  user = localStorage.getItem("token");
} catch (e) {
  console.log(e);
}

const initialState = user ? { loggedIn: true, user, isLoading: false } : {};

export function authentication(state = initialState, action) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user,
        isLoading: true,
        error: "",
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user,
        error: "",
        isLoading: false,
      };
    case userConstants.LOGIN_FAILURE:
      return { error: action.error, isLoading: false };
    case userConstants.REG_FAILURE:
      return { error: action.error, isLoading: false };
    case userConstants.LOGOUT:
      return {};
    default:
      return state;
  }
}
