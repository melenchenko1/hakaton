import { combineReducers } from "redux";

import { authentication } from "./authentication.reducer";
import { user } from "./user.reducer";
import { app } from "./app.reducer";
import { team } from "./team.reducer";
import { orgConferences } from "./orgConferences.reducer";

const rootReducer = combineReducers({
  app,
  authentication,
  user,
  team,
  orgConferences,
});

export default rootReducer;
